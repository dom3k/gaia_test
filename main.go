package main

import (
	"log"

	sdk "github.com/gaia-pipeline/gosdk"
)

// PrintVaultParam prints out all given params.
func PrintVaultParam(args sdk.Arguments) error {
	for _, arg := range args {
		log.Printf("Key: %s;Value: %s;\n", arg.Key, arg.Value)
	}
	return nil
}

func main() {
	jobs := sdk.Jobs{
		sdk.Job{
			Handler:     PrintVaultParam,
			Title:       "Print Vault Parameters",
			Description: "This job prints out all given params which includes one from the vault.",
			Args: sdk.Arguments{
				sdk.Argument{
					Type: sdk.VaultInp,
					Key:  "dbpassword",
				},
			},
		},
	}

	// Serve
	if err := sdk.Serve(jobs); err != nil {
		panic(err)
	}
}
